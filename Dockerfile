FROM nvidia/cuda:10.2-cudnn7-runtime-ubuntu18.04
LABEL maintainer="mindcraft.ai"

#ENV SPACY_VERSION 2.3

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update -y && apt upgrade -y  && apt install unzip nano wget htop jupyter-notebook -y

RUN pip3 install -U numpy pandas jupyterlab

#RUN pip install spacy[cuda102]==${SPACY_VERSION} && python3 -m spacy download en 
RUN pip3 install spacy[cuda102] && python3 -m spacy download en

EXPOSE 8888

RUN mkdir /data

WORKDIR /data

CMD ["jupyter", "lab", "--allow-root", "--notebook-dir=/data/", "--ip=0.0.0.0" ]
